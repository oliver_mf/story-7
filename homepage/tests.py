from django.test import TestCase
from django.test.client import Client
from django.urls import resolve
from django.apps import apps
from homepage.apps import HomepageConfig
from .forms import *
from .views import *
from .models import *

# Create your tests here.
class story6Tests(TestCase):
    def test_index_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_confirmation_url_exist(self):
        response = Client().get('/confirmation')
        self.assertEqual(response.status_code,200)

    def test_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_confirmation_template(self):
        response = Client().get('/confirmation')
        self.assertTemplateUsed(response, 'confirmation.html')

    def test_create_new_status(self):
        new = Status.objects.create(name = 'Name', status = 'Good')
        self.assertTrue(isinstance(new, Status))
        self.assertTrue(new.__str__(), new.name)
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    