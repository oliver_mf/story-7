from django.db import models

#Create your models here.
class Status(models.Model):
    name = models.CharField(max_length=255, default='Name')
    status = models.CharField(max_length=255, default='Status')

    def __str__(self):
        return self.name
